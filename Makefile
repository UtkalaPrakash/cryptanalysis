hw3: hw3.o
	gcc -o hw3 -g hw3.o -lcrypto

hw3.o: hw3.c
	gcc -g -c -Wall hw3.c -I/usr/local/opt/openssl/include

clean:
	rm -f *.o hw3
