
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <getopt.h>
#include <math.h>

#define MD5_DIGEST_LENGTH 16

void pusage() {
	// This prints the usage and ends the program on occurrence of error.
	fprintf(stderr, "Malformed command\n");
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t./hw2 keygen -p=pphrase -t=period\n");
	fprintf(stderr, "\t./hw2 crypt -k=keyfile [file]\n");
	fprintf(stderr, "\t./hw2 invkey keyfile\n");
	fprintf(stderr, "\t./hw2 histo -t=period -i=which [file]\n");
	fprintf(stderr, "\t./hw2 solve -l=max_t file\n");
	exit(1);
}

FILE* fileCheck(char *file_name) {
	// This function checks whether input file is readable and its existence.
	if ( access(file_name, F_OK) == -1 ) {
	    fprintf(stderr, "Input file %s doesn't exist\n", file_name);
	    exit(1);
	}

	struct stat file_check;
	stat(file_name, &file_check);
	if ( S_ISDIR(file_check.st_mode) ) {
	    fprintf(stderr, "Input file %s is a directory\n", file_name);
	    exit(1);
	}

	if ( access(file_name, R_OK) == -1 ) {
	    fprintf(stderr, "Cannot read input file %s, permission denied\n", file_name);
	    exit(1);
	}

	FILE *file_handler = fopen(file_name, "r");
	if ( file_handler == NULL) {
            fprintf(stderr, "Input file %s cannot be open\n", file_name);
            exit(1);
        }
	return file_handler;
}

void read_char(char *string, int T, FILE *fRead) {
	int i;
	for (i=0;i<T;) {
		char input;
    	fread(&input, 1, 1, fRead);
   		if (input == '\n') {
    		*string++ = input;
    		break;
    	} else if (input < 32 || input > 126) {
    		fprintf(stderr, "Input file not in right format\n");
    		exit(1);
		} else if (input > 96 && input < 123) {
			*string++ = input;
			i++;
		} else {
            string -= i;
            break;
        }
   	}
    *string = '\0';
}

void keygen(char *pphrase, int T) {
	// Modified the piece of  code given in the homework
	char md5_buf[MD5_DIGEST_LENGTH];
	int len = 0;
	int l = strlen(pphrase) + 2 + MD5_DIGEST_LENGTH;
	char *s = malloc(l+1);
	int i = 0;

	MD5((unsigned char*) pphrase, strlen(pphrase), (unsigned char*) md5_buf);
	while(len < T) {
		char state[27] = "abcdefghijklmnopqrstuvwxyz";
		int j = 2, L = 26;
		while (L > 1) {
			unsigned char key_bytes[9] = "";
			if ( i == 100 ) i = 0;
	    	sprintf(&s[MD5_DIGEST_LENGTH], "%02d%s", i, pphrase);
    		memcpy(s, md5_buf, MD5_DIGEST_LENGTH);
   			MD5((unsigned char*) s, l, (unsigned char*) md5_buf);
			memcpy(key_bytes, md5_buf, 8);
			unsigned long int x1 = 0, x2 = 0, itr =0;
			for (;itr<4; itr++) x1 += (key_bytes[itr]<<((3-itr)*8));
			for (;itr<8; itr++) x2 += (key_bytes[itr]<<((7-itr)*8));
			x1 = (x1 & 0xffffffff);
			x2 = (x2 & 0xffffffff);
			//printf("%lx => %lu\t", x1, x1);
    	   	int R = x1 % L;
    		//printf("%d\t%d\n", L, R);
   			if (R != L-1) {
       			char temp = state[R];
       			state[R] = state[L-1];
       			state[L-1] = temp;
       		}
       		L--;
           	if (L == 1) {
           		if (--j>0) {
           			L = 26;
           		} else {
       				break;
       			}
        	}
           	//printf("%lx => %lu\t", x2, x2);
           	R = x2 % L;
       		//printf("%d\t%d\n", L, R);
       		if (R != L-1) {
       			char temp = state[R];
       			state[R] = state[L-1];
       			state[L-1] = temp;
       		}
       		L--;
	       	i++;
   		}
   		printf("%s\n", state);
		len++;
    }
   	free(s);
}

int main(int argc, char **argv) {
	char option[8] = "", pphrase[100] = "", keyfile[50] = "";
	int max, opt, T, which;

	if (argc < 2 ) pusage();
    // Using my code from warmup assignments in CSCI 402
    if (	strcmp(*(argv+1), "keygen") != 0 && 
			strcmp(*(argv+1), "crypt") != 0 && 
			strcmp(*(argv+1), "invkey") != 0 && 
			strcmp(*(argv+1), "histo") != 0 && 
			strcmp(*(argv+1), "solve") != 0 ) {
	    pusage();
    } else {
	    strcpy(option, *(argv+1));
	}

	static struct option options[] = {
        	{"p",    required_argument, 0,  'p' },
        	{"t",    required_argument, 0,  't' },
        	{"k",    required_argument, 0,  'k' },
        	{"i",    required_argument, 0,  'i' },
        	{"l",    required_argument, 0,  'l' },
			{NULL, 0, NULL, 0}
	};

	// Handles input arguments
	int index = 0;
	while ((opt = getopt_long_only(argc, argv,"ptil:k:", options, &index)) != -1) {
		if ( opt == 'p' ) {
		    sprintf(pphrase, "%s", optarg);
		} else if ( opt == 't' ) {
		    T = atoi(optarg);
		} else if ( opt == 'k' ) {
		    sprintf(keyfile, "%s", optarg);
		} else if ( opt == 'l' ) {
		    max = atoi(optarg);
		} else if ( opt == 'i' ) {
		    which = atoi(optarg);
		} else {
	 	    pusage();
		}
    }

    if (strcmp(option, "keygen") == 0) {
    	if (strlen(pphrase) == 0 || T == 0) pusage();
    	keygen(pphrase, T);
    } else if (strcmp(option, "crypt") == 0) {
    	if (argc < 3 || strlen(keyfile) == 0) pusage();
    	FILE *fRead, *keyRead;
    	keyRead = fileCheck(keyfile);
    	if (argc == 4) {
    		fRead = fileCheck(*(argv+3));
    	} else {
    		fRead = stdin;
    	}

    	fseek(keyRead, 0L, SEEK_END);
    	int size = ftell(keyRead), i = 0, T = 0, match[26] = {0};
    	char key[size/27][27], input = '\0';
    	rewind(keyRead);

    	for (i = 0; i < size; i++) {
    		fread(&input, 1, 1, keyRead);
    		if (input == '\n') {
    			T++;
                if (i%27 != 26) {
                    printf("Malformed key file. Row %d doesn't have 26 characters\n", T);
                    exit(1);
                }
                int k =0;
                for(;k<26;k++) match[k] = 0;
            } else if (input < 97 || input > 122) {
                printf("Malformed key file. Special character '%c' is present in line %d.\n", input, T);
                exit(1);
    		} else {
                if (match[input-97]) {
                    printf("Malformed key file. Character '%c' is repeating in line %d\n", input, T);
                    exit(1);
                }
    			key[T][i%27] = input;
                match[input-97]++;
    		}
    	}
    	
    	i = 0;
    	fread(&input, 1, 1, fRead);
    	while (input != '\n') {
    		if (input < 32 || input > 126) {
    			fprintf(stderr, "Input file not in right format\n");
    			exit(1);
   			}
   			if (input > 96 && input < 123) {
   				int row = i % T;
    			int col = (input - 97);
    			printf("%c", key[row][col]);
    		} else {
   				printf("%c", input);
   			}
   			i++;
    		fread(&input, 1, 1, fRead);
    	}
    	printf("\n");
    } else if (strcmp(option, "invkey") == 0) {
    	if (argc < 3 ) pusage();
    	FILE *keyRead;
    	keyRead = fileCheck(*(argv+2));

    	fseek(keyRead, 0L, SEEK_END);
    	int size = ftell(keyRead), i = 0;
    	rewind(keyRead);
    	char input = '\0', Output[27] = "";

    	for (i = 0; i < (size/27); i++) {
    		int j = 0, match[26] = {0};
    		for (; j< 26; j++) {
    			fread(&input, 1, 1, keyRead);
                if (input == '\n') {
                    printf("Malformed key file. Row %d doesn't have 26 characters\n", i+1);
                    exit(1);
                } else if (input < 97 || input > 122) {
                    printf("Malformed key file. Special character %c is present in line %d\n", input, i+1);
                    exit(1);
                } else if (match[input-97]) {
                    printf("Malformed key file. %c is repeating in line %d\n", input, i+1);
                    exit(1);
                } else {
                     match[input-97]++;
    			     Output[input - 97] = j + 97;
                }
    		}
    		fread(&input, 1, 1, keyRead);
    		printf("%s\n", Output);
    	}
    } else if (strcmp(option, "histo") == 0) {
    	if (argc < 4) pusage();
    	if ( !T || !which) pusage();

    	FILE *fRead;
    	if (argc == 5) {
    		fRead = fileCheck(*(argv+4));
    	} else {
    		fRead = stdin;
    	}

    	int i = 0, match[26] = {0}, end = 0;
    	while (1) {
    		if (end == 1) break;
    		char string[T+1], input = '\0';
    		for (i=0;i<T;i++) {
    			fread(&input, 1,1, fRead);
    			string[i] = input;
    			if (input == '\n') {
    				end = 1;
    			} else if (input < 32 || input > 126) {
    				fprintf(stderr, "Input file not in right format\n");
    				exit(1);
   				}
   			}
    		if (string[which-1] && string[which-1] > 96 && string[which-1] < 123) match[(string[which-1]-97)]++;
    	}
    	end = 0;
    	for (i=0; i<26; i++) end += match[i];
    	printf("L=%d\n", end);
    	for (i=0; i<26; i++) {
    		int j, temp =-1, k = -1;
    		for (j=25; j>=0; j--) {
    			if (match[j] >= temp) {
    				temp = match[j];
    				k = j;
    			}
    		}
    		float percent = ((float)temp/end) *100;
    		printf("%c: %d (%.2f%%)\n", (k+97), temp, percent);
    		match[k] = -1;
    	}
    } else {
    	if (argc != 4 || !max) pusage();
    	FILE *fRead = fileCheck(*(argv+3));
    	fseek(fRead, 0L, SEEK_END);
    	int size = ftell(fRead), i=0;
    	rewind(fRead);

        
        /*for(i=0; i<size; i++) {
            char input = '\0';
            fread(&input, 1, 1, fRead);
            if (input == '\n') break;
            if (input > 96 && input < 123) {
                if (i%7 == 0) printf("%d=>%c\n", i, input);
            } else {
                i--;
            }
        }
        return(0);*/

        int len = 4;
        printf("Kasiski Method\n==============\n");
    	while (1) {
    		char temp1[len+1], temp2[len+1];
    		temp1[len] = '\0';
    		temp2[len] = '\0';
    		int i, j, count = 0;
    		for ( i=0; i<size; i++) {
    	        if (temp1[strlen(temp1) - 2] == '\n') break;
    			fseek(fRead, i, SEEK_SET);
    			read_char(temp1, len, fRead);
                if (strlen(temp1) == 0) continue;
    			for (j=i+len; j<size; j++) {
    				if (temp2[strlen(temp2) - 2] == '\n') break;
    				fseek(fRead, j, SEEK_SET);
    				read_char(temp2, len, fRead);
                    if (strlen(temp1) == 0) continue;
    				if (strcmp(temp1, temp2) == 0) {
    					printf("len=%d, i=%d, j=%d, j-i=%d, %s\n", len, i, j, (j-i), temp1);
    					count++;
    				}
    			}
    		}
    		if (!count) {
    			printf("len=%d, no more matches\n", len);
    			break;
    		}
    		len++;
    	}

        printf("\nAverage Index of Coincidence\n============================\n");
        fseek(fRead, 0, SEEK_SET);
        int count[26] = {};
        char input;
        len = 0;
        fread(&input, 1, 1, fRead);
        while (input != '\n') {
            if (input < 32 || input > 126) {
                fprintf(stderr, "Input file not in right format\n");
                exit(1);
            }
            if (input > 96 && input < 123) {
                count[input-97]++;
                len++;
            }
            fread(&input, 1, 1, fRead);
        }
        i =0;
        double ic = 0;
        printf("L=%d\n", len);
        for (i=0; i<26; i++) {
            ic += (count[i] * (count[i] - 1));
            printf("f('%c')=%d\n", i+97, count[i]);
        }
        ic = ic/(len * (len-1));
        printf("IC=%.9f\n", ic);
        double t = 0.00;
        for (t=1; t<=max; t++) {
            ic = ((double)(1.0/t) * ((double)((len-t)*(1.0))/(len-1)) * 0.0658);
            ic += ((double)(((t-1)*1.0)/t) * (double)((len*1.0)/(len-1)) * (double)(1.0/26));
            printf("t=%.0f, E(IC)=%.8lg\n", t, ic);
        }

        printf("\nAuto-correlation Method\n=======================\n");
        len = 1;
        while (len <= max) {
            char temp1, temp2;
            int i=0, j=i+len, count = 0;
            do {
                fseek(fRead, i, SEEK_SET);
                fread(&temp1, 1, 1, fRead);
                fseek(fRead, j, SEEK_SET);
                fread(&temp2, 1, 1, fRead);
                i++;
                j = i+len;
                if (temp1 == temp2 && temp1 > 96 && temp1 < 123) count++; 
            } while (temp1 != '\n');
            printf("t=%d, count=%d\n", len, count);
            len++;
        }
        printf("\n");
    }
	return 0;
}
